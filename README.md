# Smart Contact Security Lab
This lab aims to provide a hand-on experience with writing smart contracts. In addition, examples of 5 common smart contract vulnerabilities will shown. It helps students to understand the processes of writing safe and secure smart contracts in a decentralised environment. 

# Lab Instructions
There are two parts of this lab: 

First, an introduction to smart contracts is provided in folder `intro` to get you familiar with writing and interacting with smart contracts. 

Second, 5 common smart contract vulnerabilities will be illustrated, and to go through these you can either use an online IDE specifically "Remix" as will be explained in the intro, or you can build a local docker with all contracts deployed and ready-to-use. See below for both of these options. 

## 1. Online IDE for Smart Contracts
If you prefer to use "Remix" online IDE (found in `https://remix.ethereum.org/`), then please navigate to `online-env` folder and follow the instruction there. 

## 2. Docker contrainer for a local Blockchain and Smart Contracts IDE
To install a docker contrainer with a local Blockchain and IDE for testing, then please navigate to `docker-env` folder and follow the instruction there. 