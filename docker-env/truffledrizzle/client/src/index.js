import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Drizzle } from "drizzle";
import Fallback from "./contracts/Fallback.json";
import Bank from "./contracts/Bank.json";
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';
import { createTheme } from '@material-ui/core/styles'
// setup drizzle
const drizzle = new Drizzle({
    contracts: [Fallback, Bank],

    web3: {
        fallback: {
            type: "ws",
            url: "ws://localhost:8545",
        },
    },
});

ReactDOM.render(
    <ThemeProvider theme={createTheme()}>
        <CssBaseline />
        <App drizzle={drizzle} />
    </ThemeProvider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
