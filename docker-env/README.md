# Requirements
1. [Docker](https://docs.docker.com/get-docker/)
2. [docker-compose](https://docs.docker.com/compose/install/)

## Running the docker containers
First, you have to build the docker containers once:
```
docker-compose build
```

Subsequently, you can run the following command to start the containers:
```
docker-compose up
```
## Entrypoints
Truffle Drizzle DApp: http://localhost<br>
Remix IDE: http://localhost:3000<br>
Ganache CLI: http://localhost:8545<br>

## Instructions
All instructions for performing the lab tasks can be found on `http://localhost` once you start the contrainers.
