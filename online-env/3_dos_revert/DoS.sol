pragma solidity ^0.5.0;

contract DoS {
    address payable currentLeader;
    uint highestBid;

    function bid() public payable {
        require(msg.value > highestBid);

        require(currentLeader.send(highestBid)); // Refund the old leader, if it fails then revert

        currentLeader = msg.sender;
        highestBid = msg.value;
    }
}