## Mock decentralized bank with vulnerabilities
On this directory you will find `Bank.sol` a mock up of a decentralized banking application.

This banking app allows you to open a banking account, deposit Ether and withdraw Ether. The bank also keeps track of an interest percentage that you have to pay over loans depending on how much ether the banking contract currently holds.

The application suffers from the following all previous smart contract vulnerabilities:

1. Reentrancy
2. Fallback
3. DoS Revert
4. Forcibly Sending
5. Integer overflow

You can copy the `Bank.sol` your Remix IDE and deploy the contract. They, try and exploit the vulnerabilities that are present in the application. The vulnerabilities are explained more in depth in previous tasks (1-5). 
