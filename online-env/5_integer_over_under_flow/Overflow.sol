pragma solidity ^0.5.0;

contract Overflow {
    uint16 public debt;

    function increaseDebt(uint16 value) public {
        debt += value;
    }
}
