### Integer Overflow and Underflow

Contracts that use arithmetic operations on primitive types such as `uint` may be vulnerable to integer overflow
and underflow attacks. For example, if we have a contract that keep tracks of a debt, if that debt is increased
often enough, it will reset back to 0 due to an overflow.

```sol
contract Overflow {
    uint public debt;

    function increaseDebt(uint value) public {
        debt += value;
    }
}
```

The solution here is to check whether the operation overflowed or underflowed before updating any state.

```sol
contract Overflow {
    uint public debt;

    function increaseDebt(uint value) public {
        require(debt + value >= debt);
        debt += value;
    }
}
```

Alternatively, one can use the `SafeMath.sol` library that performs these checks automatically.

Make sure you compile and deploy `Overflow.sol` first in the Remix IDE.
Next, figure out a way to exploit it.