## Instructions
Note: make sure to go through the steps in the `intro` folder in order to get familiar with Remix IDE

Steps to complete the lab tasks:

1. Navigate to each folder in this directory starting with 1 to 6. 
2. Copy the contract code in each folder into remix, then compile and deploy it.
2. Follow the steps to understand the vulnerability and then try exploit and fix it. 
