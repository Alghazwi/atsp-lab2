### Forcibly Sending Ether to a Contract
It is possible to forcibly send Ether to a contract without triggering its fallback function. 
This is an important consideration when placing important logic in the fallback function or making calculations based on a contract's balance. 

Solidity’s selfdestruct does two things.

1. It renders the contract useless, effectively deleting the bytecode at that address.
2. It sends all the contract’s funds to a target address.

The special case here, is that if the receiving address is a contract, its fallback function does not get executed.
This means that if a contract’s function has a conditional statement that depends on that contract’s balance being below a certain amount, that statement can be potentially bypassed:

``` Solidity
contract Forcibly {
  bool youWin = false;

  function onlyNonZeroBalance() {
      require(this.balance > 0); 
      youWin = true;
  }
  // throw if any ether is received
  function() payable {
    revert();
  }
         
}
```
Due to the throwing fallback function, normally the contract cannot receive ether. 
However, if a contract selfdestructs with this contract as a target, the fallback function does not get called. 
As a result this.balance becomes greater than 0, and thus the attacker can bypass the require statement in onlyNonZeroBalanc

Make sure you compile and deploy `Forcibly.sol` first in the Remix IDE.
Next, figure out a way to exploit it.

source: https://medium.com/loom-network/how-to-secure-your-smart-contracts-6-solidity-vulnerabilities-and-how-to-avoid-them-part-2-730db0aa4834