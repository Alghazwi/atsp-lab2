pragma solidity ^0.5.0;

contract Forcibly {
  bool youWin = false;

  function onlyNonZeroBalance() {
      require(this.balance > 0); 
      youWin = true;
  }
  // throw if any ether is received
  function() payable {
    revert();
  }
         
}