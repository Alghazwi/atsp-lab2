### 1. Reentrance Attack

Re-entrancy is the bug that caused the DAO attack, one of the most famous blockchain attacks. 
Re-entrancy happens in single-thread computing environments, when the execution stack jumps or calls subroutines, before returning to the original execution. 
Contracts are vulnerable to poor execution ordering such as the following example:

![reentrance example](./reentrance.jpg)

In the example above, Contract B is a malicious contract which recursively calls A.withdraw() to deplete Contract A’s funds. 
Note that the fund extraction successfully finishes before Contract A returns from its recursive loop, and even realizes that B has extracted way above its own balance.

In this folder you will find `Reentrance.sol`, a contract that is vulnerable to this type of attack. 
Create a contract that exploits the vulnerabilitie, hence transfers ether/wei to your contract.